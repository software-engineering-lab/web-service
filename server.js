const express = require('express');
const app = express();
const initialize = require('./dbs');
const routes = require('./routes');

async function main()
{
  try {
    const db = await initialize();
    routes(app, db).listen(8080, function() {
      console.log('Running on http://0.0.0.0:8080');
    });
  }
  catch (err) {
    console.error('Failed to make all database connections!');
    console.error(err);
    process.exit(1);
  }
}

main();