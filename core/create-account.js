const bcrypt = require('bcrypt');

module.exports = async function (req, res, db) {

    const username = req.body.username;
    const password = req.body.password;

    // Check if user is exists.
    const existsUser = await db.logistbot.collection('users').find({ username: username }).toArray();
    const existsUserLen = Object.keys(existsUser).length;
    if (existsUserLen > 0) {
        return res.status(409).json({
            error: 'Username already exists.'
        });
    }

    bcrypt.hash(password, 10, function (err, hash) {
        db.logistbot.collection('users').insertOne({ username: username, password: hash }, function (err, res) {
            if (err) {
                console.error(err);
            }
            else {
                console.log(username + ' has been inserted.');
            }
        });
    });
  
    return res.status(200).json({
        message: 'Account successfully created.'
    });
}