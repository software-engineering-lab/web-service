const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = async function (req, res, db) {

    const username = req.body.username;
    const password = req.body.password;

    try {
        var existsUser = await db.logistbot.collection('users').find({ username: username }).toArray();
        var hash = existsUser[0].password;
    }
    catch (error) {
        console.error(error);
        return res.status(400).json({
            error: 'Incorrect Username or Password.'
        });
    }

    const response = await bcrypt.compare(password, hash);
    if (response) {
        jwt.sign({ username }, 'shhhhh', { expiresIn: '30s' }, (err, token) => {
            return res.status(200).json({
                message: 'Login successfully.',
                access_token: token
            });
        });
    }
    else {
        return res.status(400).json({
            error: 'Incorrect Username or Password.'
        });
    }



}
