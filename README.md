## API Doc

**Create Account (POST)**

```
0.0.0.0:8080/create-account

Body
{
  "username": "your_name",
  "password": "your_password"
}
```

**Login (POST)**

```
0.0.0.0:8080/auth

Body
{
  "username": "your_name",
  "password": "your_password"
}

Response
{
    "message": "Login successfully.",
    "access_token": "some_token"
}
```

**Access account (POST)**

```
0.0.0.0:8080/profile

Header

Authorization: "some_token"

Response
{
    "decoded": {
        "username": "your_name",
        "iat": 1539022223,
        "exp": 1539022253
    }
}
```