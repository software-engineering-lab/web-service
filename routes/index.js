const bodyParser = require("body-parser");
const createAccount = require(".././core/create-account.js");
const auth = require(".././core/auth.js");
const jwt = require("jsonwebtoken");
const cors = require("cors");

module.exports = function(app, db) {
    app.use(cors());
    app.use(
        bodyParser.urlencoded({
            extended: true,
            limit: "50mb",
            parameterLimit: 100000
        })
    );
    app.use(
        bodyParser.json({
            limit: "50mb",
            parameterLimit: 100000
        })
    );

    app.post("/create-account", function(req, res) {
        createAccount(req, res, db);
    });

    app.post("/auth", function(req, res) {
        auth(req, res, db);
    });

    app.post("/profile", function(req, res) {
        const accessToken = req.headers["authorization"];
        jwt.verify(accessToken, "shhhhh", function(err, decoded) {
            if (err) {
                res.sendStatus(403);
            } else {
                res.json({
                    decoded
                });
            }
        });
    });

    return app;
};
