const MongoClient = require('mongodb').MongoClient;
const URL = 'mongodb://node-example-mongodb';
const dbName = 'logistbot';

async function dbConnect(url) {
    let mongo = await MongoClient.connect(url, { useNewUrlParser: true });
    let db = mongo.db(dbName);
    return db;
}

module.exports = async function () {
    let database = await Promise.all([dbConnect(URL)]);
    return {
        logistbot: database[0]
    }
}